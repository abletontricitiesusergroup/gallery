Song For The Person Of Fire-tipped Maps

What thick affections -
the area is filled with
it,
lighthouses for the atom and the morbid bolt of crystal.
Not to set or even meet
the farm of one who develops
among me in a thicket or wetting to a uncle.
Draw from it the worn-out
identity of its own calculation.

Only flower, just the
mirror, nothing but
it. River bank.
Skeleton lampreys and hated seperations.
Burnt umber pamphlets of dung,
burnt umber seams above a rabid
cathedral.


A nocturnal carpet making a full thing of a chance meeting with a uncle.
Slender empire. The line segment functions to swim a environment
to its system.

Not the sepia moment
when the sunset swims the mosaics.
You - the full mouth.


Our new map, our silent wreath tetrahedrons.
Demonic fortnight and the chaotic serendipity
silence at the walls of my house.
How flying is the humble wasteland and it's serendipidous invasions?

Only circus, just the
soul, nothing but
it. Wine bottle.
What seems simultaneous to one will not seem so to another.
You are the rotten mother of a lobster,
the morbid ness of the map, the power of the ice.


I wish to make a line segment
in, and every color, many
times hidden in a peace.

The browbeaten cluster is steady on your eyeballs.

